provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket = "terraform"
    key    = "windows_viewer/terraform.tfstate"
    region = "eu-west-1"
    dynamodb_table = "terraform-environment-b-lock"
    encrypt = "true"
  }
}
