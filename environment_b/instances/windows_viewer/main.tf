data "aws_vpc" "this" {
  tags = {
    Name = var.environment
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.environment}_private_subnet*"]
  }
}

# create the EC2 instance
# this instance won't have a public IP
module "ec2" {
  source = "../../../modules/ec2_instances"

  name = var.instance_name
  instance_count = 1

  ami = var.windows_2019_ami_id
  instance_type = var.instance_type
  key_name = var.default_ssh_key_name
  monitoring = false
  vpc_security_group_ids = local.sg_to_attach
  subnet_ids = [for s in data.aws_subnet_ids.private.ids : s]

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = var.root_disk_size
    }
  ]

  disable_api_termination = false

  tags = {
    Name = var.instance_name
    project = var.environment
  }

  volume_tags = {
    Name = var.instance_name
    project = var.environment
    Snapshot = true
  }
}
