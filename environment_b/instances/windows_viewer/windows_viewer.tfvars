# Environment to be created
environment = "environment-b"

# Default ssh key
default_ssh_key_name = "tech_challenge"

# EC2
windows_2019_ami_id = "ami-03b9a7c8f0fc1808e" # Windows Server 2019 latest AMI
instance_name = "windows_viewer"
instance_type = "t3a.medium"
root_disk_size = 30
