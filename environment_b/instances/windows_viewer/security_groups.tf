locals {
  sg_to_attach = [data.aws_security_group.default.id]
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name   = "group-name"
    values = ["default"]
  }
}
