variable "environment" {
  description = "Name of the environment to be created. Used as Default resources name where needed"
  type = string
}

variable "default_ssh_key_name" {
  description = "The default ssh key name"
  type = string
}

variable "windows_2019_ami_id" {
  description = "ID of Windows Server 2019 AMI to use for the instance"
  type = string
}

variable "instance_name" {
  description = "Name for the instance"
  type = string
}

variable "instance_type" {
  description = "Type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance"
  type = string
}

variable "root_disk_size" {
  description = "Size of the root disk in gibibytes (GiB)"
}
