locals {
  # Definition of the security group attributes
  sg_to_create = [
    {
      name = var.sg_to_create
      description = "Security group for SSH access in this VPC from a secure location"
      incoming = [
        {
          # cidr_blocks = "XXX.XXX.XXX.XXX/32", # VPN IP
          cidr_blocks = "37.48.77.174/32",
          rule = "ssh-tcp",
          description = "Allow SSH access from VPN"
        },
      ]
      outgoing = [
        {
          cidr_blocks = "0.0.0.0/0",
          rule = "all-all",
          description = "Allow all outgoing traffic"
        },
      ]
    },
  ]

  # Create a list with default segurity group ID and the ssh-from-secure-location security group ID
  sg_to_attach = [data.aws_security_group.default.id, data.aws_security_group.to_attach.id]
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name   = "group-name"
    values = ["default"]
  }
}

data "aws_security_group" "to_attach" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.sg_to_create}-${var.environment}"]
  }

  depends_on = [module.sg]
}

# create the security groups base on the sg_to_create variable defined on the local block above
module "sg" {
  source = "../../../modules/security_groups"

  for_each = { for sg in local.sg_to_create : sg.name => sg }

  name        = "${each.value.name}-${var.environment}"
  description = each.value.description
  vpc_id      = data.aws_vpc.this.id

  ingress_with_cidr_blocks = [ for rule_incoming in each.value.incoming :
    rule_incoming
  ]

  egress_with_cidr_blocks = [ for rule_outgoing in each.value.outgoing :
    rule_outgoing
  ]

  tags = {
    Name = "${each.value.name}-${var.environment}"
  }
}
