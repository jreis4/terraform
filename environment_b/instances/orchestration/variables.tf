variable "environment" {
  description = "Name of the environment to be created. Used as Default resources name where needed"
  type = string
}

variable "domain" {
  description = "Domain name to be used on deployment scripts"
  type = string
}

variable "create_certificate_for_webserver_domain" {
  description = "Create a certificate for webserver domain"
  type = bool
}

variable "default_ssh_key_name" {
  description = "The default ssh key name"
  type = string
}

variable "ubuntu_20_04_ami_id" {
  description = "ID of Ubuntu Server 20.04 AMI to use for the instance"
  type = string
}

variable "instance_name" {
  description = "Name for the instance"
  type = string
}

variable "instance_type" {
  description = "Type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance"
  type = string
}

variable "root_disk_size" {
  description = "Size of the root disk in gibibytes (GiB)"
}

variable "sg_to_create" {
  description = "Name of security group to be created"
  type = string
}

variable "office_ip" {
  description = "The IP address of your office, to allow the access via HTTP/S to the webserver"
  type = string
}

variable "iam_role" {
  description = "Name for the IAM role to be created to allow the environments deployment"
  type = string
}

variable "iam_policies" {
  description = "Map with AWS policies ARNs"
  type = map(string)
}

variable "auto_deployment" {
  description = "Allows you to launch the environments once the orchestration instance is created"
  type = bool
}
