# Environment/Product to be created
environment = "environment-b"

# Default domain
domain = "aptoide.com"
create_certificate_for_webserver_domain = true

# Default ssh key
default_ssh_key_name = "aptoide2020"

# EC2
ubuntu_20_04_ami_id = "ami-08bac620dc84221eb" # Ubuntu 20.04 latest AMI
instance_name = "orchestration"
instance_type = "t3a.micro"
root_disk_size = 20

# Security groups
sg_to_create = "ssh-from-secure-location"
office_ip = "212.32.229.36"

# IAM policies
iam_role = "environments_deployment"
iam_policies = {
  administrator_access = "arn:aws:iam::aws:policy/AdministratorAccess" # required to create new cognito resources
}

# Deployment
auto_deployment = true
