# create an IAM role for this instance
module "orchestration_role" {
  source = "../../../modules/iam_role"

  trusted_role_services = [
    "ec2.amazonaws.com"
  ]

  create_role       = true
  role_name         = var.iam_role
  role_requires_mfa = false

  custom_role_policy_arns = [
    module.bucket_access_policy.arn,
    var.iam_policies.administrator_access,
  ]
}

# create an instance profile to be attached to this instance
resource "aws_iam_instance_profile" "this" {
  name = "${var.iam_role}_instance_role"
  role = module.orchestration_role.this_iam_role_name
}
