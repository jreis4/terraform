data "aws_s3_bucket" "this" {
  bucket = "terraform"
}

data "aws_iam_policy_document" "bucket" {
  statement {
    actions   = [
      "s3:GetBucketLocation",
      "s3:ListAllMyBuckets"
    ]
    resources = ["*"]
  }

  statement {
    actions   = [
      "s3:ListBucket"
    ]
    resources = [
      data.aws_s3_bucket.this.arn
    ]
  }

  statement {
    actions   = [
      "s3:*Object"
    ]
    resources = [
      "${data.aws_s3_bucket.this.arn}/*"
    ]
  }

  depends_on = [data.aws_s3_bucket.this]
}

# create the IAM policy with the permissions defined above
module "bucket_access_policy" {
  source = "../../../modules/iam_policy"

  name        = "TerraformBucketAccess"
  path        = "/"
  description = "Terraform S3 Bucket access for orchestration EC2 instance"

  policy = data.aws_iam_policy_document.bucket.json
}
