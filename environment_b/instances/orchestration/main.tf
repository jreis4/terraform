data "template_file" "user_data" {
  template = file("${path.module}/user-data.sh")

  vars = {
    domain = var.domain
    create_certificate = var.create_certificate_for_webserver_domain
    auto_deployment = var.auto_deployment
    office_ip = var.office_ip
  }
}

data "aws_vpc" "this" {
  tags = {
    Name = var.environment
  }
}

data "aws_route53_zone" "this" {
  name = var.domain
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.environment}_public_subnet*"]
  }
}

# create the EC2 instance
# this instance have a public IP
module "ec2" {
  source = "../../../modules/ec2_instances"

  name = var.instance_name
  instance_count = 1

  ami = var.ubuntu_20_04_ami_id
  instance_type = var.instance_type
  key_name = var.default_ssh_key_name
  vpc_security_group_ids = local.sg_to_attach
  subnet_ids = [for s in data.aws_subnet_ids.public.ids : s]
  iam_instance_profile = aws_iam_instance_profile.this.name
  user_data = base64encode(data.template_file.user_data.rendered)

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = var.root_disk_size
    }
  ]

  associate_public_ip_address = true
  disable_api_termination = false

  tags = {
    Name = var.instance_name
    project = var.environment
  }

  volume_tags = {
    Name = var.instance_name
    project = var.environment
    Snapshot = true
  }
}

# create an elastic IP
resource "aws_eip" "this" {
  vpc      = true
  instance = module.ec2.id[0]
}
