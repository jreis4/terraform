#!/bin/bash

# Declare variables
DOMAIN="${domain}"
CREATE_CERT="${create_certificate}"
AUTO_DEPLOYMENT="${auto_deployment}"
OFFICE_IP="${office_ip}"

# Update the OS base packages
apt update
apt dist-upgrade -y

# Install the required packages
apt install -y python3 python3-boto

# Install the AWS CLI
apt install -y unzip curl
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm awscliv2.zip awscliv2 -rf

# Install Terraform
curl "https://releases.hashicorp.com/terraform/0.14.10/terraform_0.14.10_linux_amd64.zip" -o "terraform.zip"
unzip terraform.zip
sudo cp terraform /usr/bin/terraform
rm terraform.zip

# Clone the repository code
apt install -y git
git clone -b aptoide https://jreis4@bitbucket.org/jreis4/terraform.git /opt/tech-challenge

# Replace some variables
cd /opt/tech-challenge
sed -i "s/YOUR_DOMAIN/$DOMAIN/g" environment_a/instances/webserver/webserver.tfvars
sed -i "s/CREATE_CERT/$CREATE_CERT/g" environment_a/instances/webserver/webserver.tfvars
sed -i "s/OFFICE_IP/$OFFICE_IP/g" environment_a/instances/webserver/webserver.tfvars

# If it's true, will deploy the rest of the infrastructure
if [[ $AUTO_DEPLOYMENT == true ]]; then
  /opt/tech-challenge/environment_b/scripts/deployment/create_resources.sh
fi
