provider "aws" {
  region = "eu-west-1"
  profile = "terraform"
}

terraform {
  backend "s3" {
    bucket = "terraform"
    key    = "orchestration/terraform.tfstate"
    region = "eu-west-1"
    profile = "terraform"
    dynamodb_table = "terraform-enviroment-b-lock"
    encrypt = "true"
  }
}
