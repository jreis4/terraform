#!/bin/bash

# Declare variables
OPT=$1
USER_EMAIL=$2

# Get cognito pool ID
COGNITO_POOL_ID=$(aws cognito-idp list-user-pools --max-results 1 --query UserPools[].Id --output text)

# Create a cognito user
create_user() {
  aws cognito-idp admin-create-user --user-pool-id $COGNITO_POOL_ID --username $USER_EMAIL --user-attributes Name=email,Value=$USER_EMAIL
}

# Delete a cognito user
delete_user() {
  aws cognito-idp admin-delete-user --user-pool-id $COGNITO_POOL_ID --username $USER_EMAIL
}

# script usage
usage() {
  echo 'Usage is: create_users.sh [OPTION] [USER_EMAIL]'
  echo -e 'options available:\n'
  echo '-c OR --create-user'
  echo '-d OR --delete-user'
}

# Execution
if [[ ${OPT} == '--create-user' || ${OPT} == '-c' ]]; then
  create_user
elif [[ ${OPT} == '--delete-user' || ${OPT} == '-d' ]]; then
  delete_user
else
  usage
fi

if [[ -z ${USER_EMAIL} ]]; then
  usage
fi
