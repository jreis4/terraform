#!/bin/bash

# Declare variables
REPO_PATH='/opt/tech-challenge'

# Destroy windows viewer
cd ${REPO_PATH}/environment_b/instances/windows_viewer
terraform init
terraform destroy -var-file=windows_viewer.tfvars -auto-approve

# Destroy webserver
cd ${REPO_PATH}/environment_a/instances/webserver
terraform init
terraform destroy -var-file=webserver.tfvars -auto-approve

# Disable peering connection between VPCs
cd ${REPO_PATH}/environment_a/vpc/vpc_peering
terraform init
terraform destroy -var-file=vpc_peering.tfvars -auto-approve

# Destroy VPC for environment A
cd ${REPO_PATH}/environment_a/vpc/vpc_creation
terraform init
terraform destroy -var-file=vpc_creation.tfvars -auto-approve
