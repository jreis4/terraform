#!/bin/bash

# Declare variables
REPO_PATH='/opt/tech-challenge'

# Create VPC for environment A
cd ${REPO_PATH}/environment_a/vpc/vpc_creation
terraform init
terraform apply -var-file=vpc_creation.tfvars -auto-approve

# Enable peering connection between VPCs
cd ${REPO_PATH}/environment_a/vpc/vpc_peering
terraform init
terraform apply -var-file=vpc_peering.tfvars -auto-approve

# Create webserver
cd ${REPO_PATH}/environment_a/instances/webserver
terraform init
terraform apply -var-file=webserver.tfvars -auto-approve

# Create windows viewer
cd ${REPO_PATH}/environment_b/instances/windows_viewer
terraform init
terraform apply -var-file=windows_viewer.tfvars -auto-approve
