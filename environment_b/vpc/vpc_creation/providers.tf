provider "aws" {
  region = "eu-west-1"
  profile = "terraform"
}

terraform {
   backend "s3" {
     bucket = "terraform"
     key    = "vpc_env_b/terraform.tfstate"
     region = "eu-west-1"
     profile = "terraform"
     dynamodb_table = "terraform-environment-b-lock"
     encrypt = "true"
   }
}
