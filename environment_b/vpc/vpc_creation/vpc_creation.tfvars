environment = "environment-b"

# Default ssh key
default_ssh_key_name = "tech_challenge"
default_ssh_key_public = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyS2N8OcosuPzSrxRUDJeuk8lJY84d1gNi2GL7jEdacDtwH7jG1VVacI9iMNJ6KhyXzljm6/IXDIp2txlWDWHW0XuyyE1UHtTEmpj04ppG3Pu3szMsNkujTA8SELKXq61n4ewx/z4cRJTkI+0G87ls9AkstULK7aYxfYlVCC47q6eCRz9feX3wonV34hs+3kvaQehggpW5B6IqB8pByyULB240DgYUamlxYhTCxKWhwLfflfU6xMnpy5b49cd7gSIMZtl+UcS3SsRH/URnHIgpBJpt5u0Qg6PtO8EOZJcWRiPtG972t8oxom+QcVSFYLZLa5idtQ4pMwnIGy+z9BdU+I3CKQ9TEVK815V33ClpswM5c5C/8tCOXX+vlnKUXT1yYWhWB6D/gwpP9c4o02OtJR4oB2Btv2wGhEJCZovwgXTF+Rr23IctOFusV+YrTf/5ofCLGEUkNyNmZXpT+M2AWVrTQHqoegBnTrXUsGZ7WThKXQzdIqX0EAaSa7QwQmE2F/mbK/Pu205E9YAPzYhM0dZin4I8RYPL1VAYynL19TqKguwTVdbmfWWtmYdPZMmKdeuyg2EYRLI6kNNZ/jMv6jTXYcVC/dYDfOez+zOUYG2wZrODt1RfQZglGXKxGKL4O3qniOt5qKUKV4Czu7xi7+uDVbXu8353r6ID7VrUpQ== tech_challenge"

# Network ranges for this VPC
vpc_cidr = "10.0.0.0/16"
public_subnet_cidrs = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
private_subnet_cidrs = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
