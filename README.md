# Terraform
Terraform scripts for IaC deployment

## Required tools

### AWS CLI
Please follow the instructions [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) to install the AWS CLI

### Terraform
Please follow the instructions [here](https://www.terraform.io/downloads.html) to install Terraform

**NOTE:** This challenge was created and tested using with Terraform v14

### Programmatic Access for Terraform

You need to create a IAM user with Programmatic access for terraform to allow the tool to create the VPC for "Environment B" and the orchestration EC2 instance

For that, please access to your AWS Console Management, go to **Services > IAM > Access Management > Users**, click on **Add user** button, fill **User name** field (e.g. terraform) and select the *Programmatic access* option on **Access type** field. On **Set permissions**, click on **Attach existing policies directly** option and select *AmazonVPCFullAccess*, *AmazonEC2FullAccess* policies. Add tags if you want, and click on **Create user** button to finish the creation process. After that you will get the *Access Key ID* and *Secret Access Key*, copy them.

### Configure AWS CLI with Terraform user credentials
Run the command below
```bash
aws configure
```
It'll ask you for some information like the *Access Key ID*, *Secret Access Key*, *Region* (in this challenge we're going to use *eu-west-1*) and *Text Output*.

We recommend to edit your *.aws/credentials* file and rename the **default** profile to **terraform**.

**NOTE:** if you have others AWS Programmatic Accesses configured on your AWS CLI, please edit the your *.aws/credentials* file and create a new profile called terraform with the *Access Key ID* and *Secret Access Key* information.

## First steps

### Create an S3 bucket and DynamoDB tables for terraform use
Terraform will require a S3 bucket to store the states and a dynamodb lock table for each environment.

To create those resources, please go to *first_run* folder and run the following commands:
```bash
terraform init
terraform plan # validate the creation of the S3 buckets and 2 DynamoDB tables
terraform apply # create the resources
```

### SSH key to access the EC2 instances
Created a SSH key locally with the command below:
```bash
ssh-keygen -t rsa -b 4096 -C "tech_challenge" -f /tmp/tech_challenge
```

### Adjust some variables
Replace the value of the **default_ssh_key_public** variable at *environment_b/instances/orchestration/orchestration.tfvars* file with the content of the tech_challenge public key (/tmp/tech_challenge.pub)
```bash
cat /tmp/tech_challenge.pub
```

Also you need to edit the *environment_b/instances/orchestration/orchestration.tfvars* file and setup the values correctly for the variables below:
- domain: replace the default with your domain
- create_certificate_for_webserver_domain: if it's true will create an AWS certificate for the webserver domain
- office_ip: replace with your IP
- auto_deployment: if it's true will launch all the resources of the environment A and the windows viewer instance on environment B

## Deploy the environments

### Create VPC for environment B
Go to *environment_b/vpc/vpc_creation* folder, and run the following command:
```bash
terraform init
terraform plan # validate the creation of the VPC
terraform apply # create the resources
```

### Deploy orchestration instance
Once the VPC is created, you can create the orchestration instance.

**NOTE:** This instance will deploy the rest of the infrastructure automatically if you set the **auto_deployment** variable to **true**, otherwise you need to access via SSH to the instance and run the **/opt/tech-challenge/environment_b/scripts/deployment/create_resources.sh** script.

To deploy this instance, please run:
```bash
terraform init
terraform plan # validate the creation of the instance
terraform apply # create the resources
```

Once the terraform process finishes, you should wait around 10-20 minutes to the orchestration instance launch all the remain resources.

## Allow users to view the webserver page

### Create users
Access via SSH to the orchestration instance and run the following command:
```bash
/opt/tech-challenge/environment_b/scripts/users_authentication/create_users.sh -c <user_email>
```

### Delete users
Access via SSH to the orchestration instance and run the following command:
```bash
/opt/tech-challenge/environment_b/scripts/users_authentication/create_users.sh -d <user_email>
```

## Destroy the environments

### Destroy the resources
To destroy the Webserver, Windows Viewer and VPC for environment A, please access via SSH to the orchestration instance and run the **/opt/tech-challenge/environment_b/scripts/deployment/destroy_resources.sh** script.

Once the script finishes to destroy all the resources, you can proceed to destroy the orchestration instance.

### Destroy the orchestration instance
To destroy this instance, please run:
```bash
terraform destroy # destroy the resources
```

### Destroy VPC for environment B
Go to *environment_b/vpc/vpc_creation* folder, and run the following command:
```bash
terraform destroy # destroy the resources
```
