# Modules
Common modules used for terraform scripts for IaC deployment

Based on scripts from:

For VPC creation:
https://github.com/arminc/terraform-ecs

For route53 zones and record creation:
https://github.com/terraform-aws-modules/terraform-aws-route53 -> see example https://github.com/terraform-aws-modules/terraform-aws-route53/tree/master/examples/complete
Renamed folder to route53_zones and route53_zones

For VPC Peering creation:
https://github.com/cloudposse/terraform-aws-vpc-peering

For EC2 instance creation:
https://github.com/terraform-aws-modules/terraform-aws-ec2-instance

For ALB creation:
https://github.com/terraform-aws-modules/terraform-aws-alb

For Security Groups creation:
https://github.com/terraform-aws-modules/terraform-aws-security-group

For AutoScaling Groups creation:
https://github.com/terraform-aws-modules/terraform-aws-autoscaling

For IAM users creation:
https://github.com/terraform-aws-modules/terraform-aws-iam

For s3 buckets creation:
https://github.com/terraform-aws-modules/terraform-aws-s3-bucket
