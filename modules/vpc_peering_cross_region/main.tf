# Acceptor's credentials
provider "aws" {
  alias   = "acceptor"
  region  = var.acceptor_vpc_region
  profile = var.provider_profile
}

# Requestors's credentials
provider "aws" {
  alias   = "requestor"
  region  = var.requestor_vpc_region
  profile = var.provider_profile
}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.17.0"
  enabled    = var.enabled
  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = var.attributes
  tags       = var.tags
}

resource "aws_vpc_peering_connection" "default" {
  count       = var.enabled ? 1 : 0
  vpc_id      = join("", data.aws_vpc.requestor.*.id)
  peer_vpc_id = join("", data.aws_vpc.acceptor.*.id)
  peer_region = var.acceptor_vpc_region
  auto_accept = false

  tags = module.label.tags

  timeouts {
    create = var.create_timeout
    update = var.update_timeout
    delete = var.delete_timeout
  }
}

# Lookup requestor VPC so that we can reference the CIDR
data "aws_vpc" "requestor" {
  count = var.enabled ? 1 : 0
  id    = var.requestor_vpc_id
  tags  = var.requestor_vpc_tags
}

# Lookup acceptor VPC so that we can reference the CIDR
data "aws_vpc" "acceptor" {
  provider = aws.acceptor

  count = var.enabled ? 1 : 0
  id    = var.acceptor_vpc_id
  tags  = var.acceptor_vpc_tags
}

data "aws_route_tables" "requestor" {
  count  = var.enabled ? 1 : 0
  vpc_id = join("", data.aws_vpc.requestor.*.id)
  tags   = var.requestor_route_table_tags
}

data "aws_route_tables" "acceptor" {
  provider = aws.acceptor

  count  = var.enabled ? 1 : 0
  vpc_id = join("", data.aws_vpc.acceptor.*.id)
  tags   = var.acceptor_route_table_tags
}

# Create routes from requestor to acceptor
resource "aws_route" "requestor" {
  count                     = var.enabled ? length(distinct(sort(data.aws_route_tables.requestor.0.ids))) * length(data.aws_vpc.acceptor.0.cidr_block_associations) : 0
  route_table_id            = element(distinct(sort(data.aws_route_tables.requestor.0.ids)), ceil(count.index / length(data.aws_vpc.acceptor.0.cidr_block_associations)))
  destination_cidr_block    = data.aws_vpc.acceptor.0.cidr_block_associations[count.index % length(data.aws_vpc.acceptor.0.cidr_block_associations)]["cidr_block"]
  vpc_peering_connection_id = join("", aws_vpc_peering_connection.default.*.id)
  depends_on                = [data.aws_route_tables.requestor, aws_vpc_peering_connection.default]
}

# Create routes from acceptor to requestor
resource "aws_route" "acceptor" {
  provider = aws.acceptor

  count                     = var.enabled ? length(distinct(sort(data.aws_route_tables.acceptor.0.ids))) * length(data.aws_vpc.requestor.0.cidr_block_associations) : 0
  route_table_id            = element(distinct(sort(data.aws_route_tables.acceptor.0.ids)), ceil(count.index / length(data.aws_vpc.requestor.0.cidr_block_associations)))
  destination_cidr_block    = data.aws_vpc.requestor.0.cidr_block_associations[count.index % length(data.aws_vpc.requestor.0.cidr_block_associations)]["cidr_block"]
  vpc_peering_connection_id = join("", aws_vpc_peering_connection.default.*.id)
  depends_on                = [data.aws_route_tables.acceptor, aws_vpc_peering_connection.default]
}

resource "aws_vpc_peering_connection_accepter" "acceptor" {
  provider = aws.acceptor

  count                     = var.enabled ? 1 : 0
  vpc_peering_connection_id = join("", aws_vpc_peering_connection.default.*.id)
  auto_accept               = var.auto_accept
}

resource "aws_vpc_peering_connection_options" "requestor" {
  vpc_peering_connection_id = join("", aws_vpc_peering_connection.default.*.id)

  requester {
    allow_remote_vpc_dns_resolution = var.requestor_allow_remote_vpc_dns_resolution
  }

  depends_on = [aws_vpc_peering_connection.default]
}

resource "aws_vpc_peering_connection_options" "acceptor" {
  provider = aws.acceptor

  vpc_peering_connection_id = join("", aws_vpc_peering_connection.default.*.id)

  accepter {
    allow_remote_vpc_dns_resolution = var.acceptor_allow_remote_vpc_dns_resolution
  }

  depends_on = [aws_vpc_peering_connection.default]
}
