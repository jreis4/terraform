resource "aws_route53_zone" "this" {
  count = var.create ? length(var.zones) : 0

  name          = var.zones[count.index]
  comment       = "${var.zones[count.index]}-${var.project}"
  force_destroy = var.force_destroy 

  tags = {
	project = var.project
  }
}
