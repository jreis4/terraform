variable "create" {
  description = "Whether to create Route53 zones"
  type        = bool
  default     = true
}

variable "force_destroy" {
  description = "Whether to destroy all records (possibly managed outside of Terraform) in the zone when destroying the zone"
  type        = bool
  default     = false
}

variable "zones" {
  description = "List of Route53 zones to create"
  type        = list 
}

variable "project" {
	description = "Nome of the project to use as tag"
}
