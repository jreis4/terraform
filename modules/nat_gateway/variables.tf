variable "subnet_ids" {
  type        = list
  description = "List of subnets in which to place the NAT Gateway"
}

variable "subnet_count" {
  description = "Size of the subnet_ids. This needs to be provided because: value of 'count' cannot be computed"
}

variable "tags" {
  description = "A mapping of tags to assign to elastic ip"
  type        = map(string)
  default     = {}
}
