# create a S3 bucket to store the terraform states
resource "aws_s3_bucket" "tfstatebucket" {
  bucket = "terraform"
  acl    = "private"

  # We want to have versioning enabled, because it allows us to keep track of
  # the Terraform state history
  versioning {
    enabled = true
  }

  # We also want to make sure our bucket enables server-side encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = "Terraform state bucket"
  }
}

# creating the dynamodb lock table for terraform - Environment A
resource "aws_dynamodb_table" "dynamodb-terraform-environment-a-lock" {
   name = "terraform-environment-a-lock"
   hash_key = "LockID"
   read_capacity = 20
   write_capacity = 20

   attribute {
      name = "LockID"
      type = "S"
   }

   tags = {
     Name = "Terraform Environment A Lock Table"
     project = "environment-a"
   }
}

# creating the dynamodb lock table for terraform - Environment B
resource "aws_dynamodb_table" "dynamodb-terraform-environment-b-lock" {
   name = "terraform-environment-b-lock"
   hash_key = "LockID"
   read_capacity = 20
   write_capacity = 20

   attribute {
      name = "LockID"
      type = "S"
   }

   tags = {
     Name = "Terraform Environment B Lock Table"
     project = "environment-b"
   }
}
