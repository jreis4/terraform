variable "environments" {
  description = "Map with names of the environment to connect with VPC peering"
  type = map
}

variable "vpc_peering_name" {
  description = "Name for peering connection between VPCs"
  type = string
}
