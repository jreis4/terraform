provider "aws" {
  region = "eu-west-1"
}

terraform {
   backend "s3" {
     bucket = "terraform"
     key    = "vpc_peering/terraform.tfstate"
     region = "eu-west-1"
     dynamodb_table = "terraform-environment-a-lock"
     encrypt = "true"
   }
}
