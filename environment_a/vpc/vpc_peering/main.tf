data "aws_vpc" "acceptor" {
  tags = {
    Name = var.environments.acceptor
  }
}

data "aws_vpc" "requestor" {
  tags = {
    Name = var.environments.requestor
  }
}

# VPC peering environment B to environment A
module "vpc_peering" {
  source                                    = "../../../modules/vpc_peering"
  name                                      = var.vpc_peering_name
  auto_accept                               = true
  requestor_allow_remote_vpc_dns_resolution = true
  acceptor_allow_remote_vpc_dns_resolution  = true
  requestor_vpc_id                          = data.aws_vpc.requestor.id
	acceptor_vpc_id                           = data.aws_vpc.acceptor.id
  create_timeout                            = "5m"
  update_timeout                            = "5m"
  delete_timeout                            = "10m"

  tags = {
    Name = var.vpc_peering_name
  }
}
