environments = {
  requestor = "environment-b"
  acceptor = "environment-a"
}

# VPC
vpc_peering_name = "peering-between-environments"
