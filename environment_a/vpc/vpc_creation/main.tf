# This module allow us to create a VPC with 3 public subnets and 3 private subnets
module "network" {
	source = "../../../modules/network/"

	environment          = var.environment
	vpc_cidr             = var.vpc_cidr
	public_subnet_cidrs  = var.public_subnet_cidrs
	private_subnet_cidrs = var.private_subnet_cidrs
	availability_zones   = var.availability_zones
	depends_id           = ""

  tags = {
    project = var.environment
  }
}
