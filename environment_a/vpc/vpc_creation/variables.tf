variable "environment" {
  description = "Name of the environment to be created. Used as Default resources name where needed"
  type = string
}

variable "default_ssh_key_name" {
  description = "The default ssh key name"
  type = string
}

variable "vpc_cidr" {
	description = "Main VPC CIDR"
  type = string
}

variable "public_subnet_cidrs" {
	description = "List of public subnets (with internet gateway). The list must have the same size as private_subnet_cidrs and availability_zones"
	type = list(string)
}

variable "private_subnet_cidrs" {
	description = "List of private subnets (without internet gateway). The list must have the same size as public_subnet_cidrs and availability_zones"
	type = list(string)
}

variable "availability_zones" {
	description = "Availability zones where each subnet will exist. The list must have the same size as private_subnet_cidrs and public_subnet_cidrs"
	type = list(string)
}
