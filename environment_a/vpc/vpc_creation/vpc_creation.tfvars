environment = "environment-a"

# Default ssh key
default_ssh_key_name = "tech_challenge"

# Network ranges for this VPC
vpc_cidr = "10.10.0.0/16"
public_subnet_cidrs = ["10.10.0.0/24", "10.10.1.0/24", "10.10.2.0/24"]
private_subnet_cidrs = ["10.10.10.0/24", "10.10.11.0/24", "10.10.12.0/24"]
availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
