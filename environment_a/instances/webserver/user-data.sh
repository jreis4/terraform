#!/bin/bash

# Declare variables

# Update the OS base packages
apt update
apt dist-upgrade -y

# Install Nginx
apt install -y nginx

# Set the "hello world" message at index file
mkdir -p /var/www/site/
cat > /var/www/site/index.html << EOF
<html>
	<h1>Hello world</h1>
</html>
EOF
chown -R www-data. /var/www/site

# Create vhost configuration file
cat > /etc/nginx/sites-enabled/site.conf << EOF
server {
	listen 80;
	listen [::]:80;

	server_name _;

  location / {
    root /var/www/site;
  }
}
EOF

# Remove default vhost
rm /etc/nginx/sites-enabled/default

# Reload Nginx service
systemctl reload nginx
