variable "environment" {
  description = "Name of the environment to be created. Used as Default resources name where needed"
  type = string
}

variable "domain" {
  description = "Domain name to be used on deployment scripts"
  type = string
}

variable "create_certificate_for_webserver_domain" {
  description = "Create a certificate for webserver domain"
  type = bool
}

variable "dns_endpoint" {
  description = "DNS endpoint to access the webserver"
  type = string
}

variable "default_ssh_key_name" {
  description = "The default ssh key name"
  type = string
}

variable "ubuntu_20_04_ami_id" {
  description = "ID of Ubuntu Server 20.04 AMI to use for the instance"
  type = string
}

variable "instance_name" {
  description = "Name for the instance"
  type = string
}

variable "instance_type" {
  description = "Type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance"
  type = string
}

variable "root_disk_size" {
  description = "Size of the root disk in gibibytes (GiB)"
}

variable "min_size" {
  description = "The minimum size of the autoscaling group"
  type = number
}

variable "max_size" {
  description = "The maximum size of the autoscaling group"
  type = number
}

variable "desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the autoscaling group"
  type = number
}

variable "termination_policies" {
  description = "Policy to decide how the instances in the Auto Scaling Group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, OldestLaunchTemplate, AllocationStrategy, Default"
  type = string
}

variable "policy_name" {
  description = "The name of the policy"
  type = string
}

variable "policy_target_value" {
  description = "The target value for the metric"
  type = number
}

variable "policy_metric_name" {
  description = "The name of the metric"
  type = string
}

variable "lb_health_check" {
  description = "Destination for the health check request"
  type = string
}

variable "sg_to_create" {
  description = "Name of security group to be created"
  type = string
}

variable "company_ips" {
  description = "List of company IPs distributed by office"
  type = list
}
