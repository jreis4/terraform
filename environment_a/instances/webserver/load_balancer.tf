data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.environment}_public_subnet*"]
  }
}

# create a load balancer
module "lb" {
  source = "../../../modules/alb"

  name = var.environment
  vpc_id = data.aws_vpc.this.id
  security_groups = local.sg_to_attach.lb
  subnets = [for s in data.aws_subnet_ids.public.ids : s]

  enable_deletion_protection = false

  # configure the authentication rule at the lb
  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      action_type        = "authenticate-cognito"
      target_group_index = 0
      certificate_arn    = data.aws_acm_certificate.this.arn
      authenticate_cognito = {
        authentication_request_extra_params = {
          display = "page"
          prompt  = "login"
        }
        on_unauthenticated_request = "authenticate"
        session_cookie_name        = "session-${random_pet.this.id}"
        session_timeout            = 3600
        user_pool_arn              = aws_cognito_user_pool.this.arn
        user_pool_client_id        = aws_cognito_user_pool_client.this.id
        user_pool_domain           = aws_cognito_user_pool_domain.this.domain
      }
    },
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  # create a target group to redirect the requests from the lb to the instances
  target_groups = [
    {
      name                  = var.instance_name
      backend_protocol      = "HTTP"
      backend_port          = 80
      target_type           = "instance"
      deregistration_delay  = 3
      health_check = {
        enabled             = true
        interval            = 30
        path                = var.lb_health_check
        port                = "traffic-port"
        healthy_threshold   = 5
        unhealthy_threshold = 3
        timeout             = 5
        protocol            = "HTTP"
      }
    }
  ]

  tags = {
    Name = var.environment
  }

  depends_on = [data.aws_acm_certificate.this]
}
