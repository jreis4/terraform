# Environment to be created
environment = "environment-a"

# Default domain
domain = "YOUR_DOMAIN"
create_certificate_for_webserver_domain = CREATE_CERT
dns_endpoint = "webserver"

# Default ssh key
default_ssh_key_name = "tech_challenge"

# EC2
ubuntu_20_04_ami_id = "ami-08bac620dc84221eb" # Ubuntu 20.04 latest AMI
instance_name = "webserver"
instance_type = "t3a.micro"
root_disk_size = 10

# AutoScaling Groups
min_size = 2
max_size = 50
desired_capacity = 2
termination_policies = "OldestInstance"
policy_name = "Scale instances per CPU"
policy_target_value = 75
policy_metric_name = "CPUUtilization"

# Load balancer
lb_health_check = "/"

# Security Groups
sg_to_create = "webserver-https-office"
company_ips = [
  {
    id = "Office"
    ip: "OFFICE_IP/32"
  }
]
