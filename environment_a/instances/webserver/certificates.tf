data "aws_acm_certificate" "this" {
  domain = "${var.dns_endpoint}.${var.domain}"
  depends_on = [module.acm_certificates]
}

# create certificate for DNS endpoint
module "acm_certificates" {
  source = "../../../modules/acm_certificates"
	count  = (var.create_certificate_for_webserver_domain == true) ? 1 : 0

	base_domain = var.domain
  domain_name = "${var.dns_endpoint}.${var.domain}"

	validation_record_ttl             = 60
	allow_validation_record_overwrite = true

	tags = {
		project = var.environment
	}
}
