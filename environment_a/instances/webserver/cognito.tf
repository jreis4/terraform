# Define the authentication service (Cognito) to allow the authenticated users to view the webserver page
resource "random_pet" "this" {
  length = 2
}

resource "aws_cognito_user_pool" "this" {
  name = "user-pool-${random_pet.this.id}"

  auto_verified_attributes = ["email"]
  username_attributes      = ["email"]

  account_recovery_setting {
    recovery_mechanism {
      name      = "verified_email"
      priority  = 1
    }
  }
}

resource "aws_cognito_user_pool_client" "this" {
  name                                 = "user-pool-client-${random_pet.this.id}"
  user_pool_id                         = aws_cognito_user_pool.this.id
  generate_secret                      = true
  allowed_oauth_flows                  = ["code", "implicit"]
  callback_urls                        = ["https://${var.dns_endpoint}.${var.domain}/oauth2/idpresponse"]
  allowed_oauth_scopes                 = ["email", "openid"]
  allowed_oauth_flows_user_pool_client = true
  supported_identity_providers         = ["COGNITO"]
}

resource "aws_cognito_user_pool_domain" "this" {
  domain       = random_pet.this.id
  user_pool_id = aws_cognito_user_pool.this.id
}
