data "aws_route53_zone" "this" {
  name = var.domain
}

# create a DNS record to access the webserver
module "route53_record_lb" {
  source = "../../../modules/route53_records"

  zone_id = data.aws_route53_zone.this.zone_id

  records = [
    {
      name    = var.dns_endpoint
      type    = "A"
      alias   = {
        name    = module.lb.this_lb_dns_name
        zone_id = module.lb.this_lb_zone_id
      }
    },
  ]

  depends_on = [module.lb]
}
