data "template_file" "user_data" {
  template = file("${path.module}/user-data.sh")

  vars = {}
}

data "aws_vpc" "this" {
  tags = {
    Name = var.environment
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.environment}_private_subnet*"]
  }
}

# create an autoscaling group to allow the webserver have high availability
module "autoscaling_group" {
  source  = "../../../modules/asg"

  name = var.instance_name
  create_lc = false

  # create a launch template
  lt_name              = var.instance_name
  image_id             = var.ubuntu_20_04_ami_id
  user_data            = base64encode(data.template_file.user_data.rendered)
  instance_type        = var.instance_type
  key_name             = var.default_ssh_key_name
  security_groups      = local.sg_to_attach.ec2
  target_group_arns    = module.lb.target_group_arns
  enable_monitoring    = false

  # Auto scaling group
  asg_name                  = "${var.environment}-${var.instance_name}"
  vpc_zone_identifier       = [for s in data.aws_subnet_ids.private.ids : s]
  health_check_type         = "EC2"
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = 0
  termination_policies      = [var.termination_policies]

  tags = [
    {
      key                 = "project"
      value               = var.environment
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = var.instance_name
      propagate_at_launch = true
    },
  ]
}

# define the metrics that allow the service scale
resource "aws_autoscaling_policy" "cpu_utilization" {
  name               = var.policy_name
  policy_type        = "TargetTrackingScaling"
	autoscaling_group_name = module.autoscaling_group.this_autoscaling_group_name

	target_tracking_configuration {
		customized_metric_specification {

      metric_dimension {
        name  = "AutoScalingGroupName"
        value = module.autoscaling_group.this_autoscaling_group_name
      }

			metric_name = var.policy_metric_name
			namespace = "AWS/EC2"
			statistic = "Maximum"
			unit = "Percent"
		}

		target_value = var.policy_target_value
	}
}
