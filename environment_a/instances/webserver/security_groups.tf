locals {
  http_rules = [
    {
      id = "HTTP"
      rule = "http-80-tcp"
    },
    {
      id = "HTTPS"
      rule = "https-443-tcp"
    }
  ]

  sg_to_create = [
    {
      name = var.sg_to_create
      description = "Security group for webserver with HTTP and HTTPS ports open only to company IPs"
      incoming = flatten([ for office in var.company_ips :
        [ for r in local.http_rules :
          {
            cidr_blocks = office.ip,
            rule = r.rule,
            description = "Allow ${r.id} from ${office.id}"
          }
        ]
      ])
      outgoing = [
        {
          cidr_blocks = "0.0.0.0/0",
          rule = "all-all",
          description = "Allow all outgoing traffic"
        },
      ]
    },
    {
      name = "ssh-from-orchestration"
      description = "Security group for SSH accesses"
      incoming = [
        {
          cidr_blocks = "10.0.2.132/32"
          rule = "ssh-tcp",
          description = "Allow ssh from orchestration"
        }
      ]
      outgoing = [
        {
          cidr_blocks = "0.0.0.0/0",
          rule = "all-all",
          description = "Allow all outgoing traffic"
        },
      ]
    },
  ]

  sg_to_attach = {
    lb = [data.aws_security_group.default.id, data.aws_security_group.to_attach.id]
    ec2 = [data.aws_security_group.default.id, data.aws_security_group.temp.id]
  }
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name   = "group-name"
    values = ["default"]
  }
}

data "aws_security_group" "to_attach" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["${var.sg_to_create}-${var.environment}"]
  }

  depends_on = [module.sg]
}

data "aws_security_group" "temp" {
  vpc_id = data.aws_vpc.this.id

  filter {
    name = "tag:Name"
    values = ["ssh-from-orchestration-${var.environment}"]
  }

  depends_on = [module.sg]
}

# create the security groups base on the sg_to_create variable defined on the local block above
module "sg" {
  source = "../../../modules/security_groups"

  for_each = { for sg in local.sg_to_create : sg.name => sg }

  name        = "${each.value.name}-${var.environment}"
  description = each.value.description
  vpc_id      = data.aws_vpc.this.id

  ingress_with_cidr_blocks = [ for rule_incoming in each.value.incoming :
    rule_incoming
  ]

  egress_with_cidr_blocks = [ for rule_outgoing in each.value.outgoing :
    rule_outgoing
  ]

  tags = {
    Name = "${each.value.name}-${var.environment}"
  }
}
